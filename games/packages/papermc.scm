;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 bqv <bqv@fron.io>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages papermc)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages java))

(define* (make-papermc
           #:key (mc-version "1.16.5")
                 (build-num "777")
                 (src-hash (base32 "0pp6dxn53aw6iiy9wnbkkmg8mcqpx8wy6nify8j4hrzvzrmks82c")))
  (package
    (name "papermc")
    (version (string-append mc-version "r" build-num))
    (source (origin
              (method url-fetch)
              (uri (string-append "https://papermc.io/api/v1/paper/"
                                  mc-version
                                  "/"
                                  build-num
                                  "/download"))
              (sha256 src-hash)))
    (build-system trivial-build-system)
    (inputs `(("openjdk" ,openjdk16)
              ("bash"    ,bash)))
    (arguments
      `(#:modules ((guix build utils))
        #:builder (begin
                    (use-modules (guix build utils)
                                 (srfi srfi-26))
                    (let* ((source   (assoc-ref %build-inputs "source"))
                           (patchelf (assoc-ref %build-inputs "patchelf"))
                           (openjdk  (assoc-ref %build-inputs "openjdk"))
                           (bash     (assoc-ref %build-inputs "bash"))
                           (output   (assoc-ref %outputs "out")))
                      (mkdir-p (string-append output "/share/papermc"))
                      (copy-file source (string-append output "/share/papermc/papermc.jar"))
                      (mkdir-p (string-append output "/bin")) 
                      ;; TODO: replace this with lisp (use install-jars, etc)
                      (let ((port (open-file (string-append output "/bin/minecraft-server") "a")))
                        (display (string-append "#!" (string-append bash "/bin/sh") "\n") port)
                        (display (string-append "exec "
                                                (string-append openjdk "/bin/java ")
                                                "$@ "
                                                "-jar "
                                                (string-append output "/share/papermc/papermc.jar ")
                                                "nogui "
                                                "\n") port)
                        ;; TODO: use some syntax sugar like "with-output-to-port"?
                        (close port))
                      (chmod (string-append output "/bin/minecraft-server") #o555) 
                      #t))))
    (home-page "https://papermc.io")
    (synopsis "High-performance Minecraft Server")
    (description "High-performance Minecraft Server")
    (license license:gpl3)))

(define-public papermc-r771
  (make-papermc
    #:mc-version "1.16.5"
    #:build-num "771"
    #:src-hash (base32 "1lmlfhigbzbkgzfq6knglka0ccf4i32ch25gkny0c5fllmsnm08l")))

(define-public papermc
  (make-papermc))
