;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Giacomo Leidi <goodoldpaul@autistici.org>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages a-short-hike)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mono)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (ice-9 string-fun)
  #:use-module (nonguix licenses))

(define-public gog-a-short-hike
  (let ((lib-dir "AShortHike_Data/MonoBleedingEdge/x86_64")
        (plugin-dir "AShortHike_Data/Plugins"))
    (package
      (name "gog-a-short-hike")
      (version "1.8.14")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://a_short_hike/en3installer0")
         (file-name "a_short_hike_1_8_14_44991.sh")
         (sha256
          (base32
           "0ccql1n8kxjngjswl3h1pcp08jhhl0vzzjgcmz8kwcank0prhnnf"))))
      (supported-systems '("x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `(("AShortHike.x86_64"
            ("libc" "gcc:lib"))
           ("UnityPlayer.so"
            ("libc" "gcc:lib"
             "eudev" "mesa"
             "pulseaudio"
             "libxext" "libx11" "libxcursor" "libxinerama" "libxi"
             "libxrandr" "libxscrnsaver" "libxxf86vm"))
           (,,(string-append lib-dir "/libmonobdwgc-2.0.so")
            ("libc" "gcc:lib" "mono"))
           (,,(string-append lib-dir "/libMonoPosixHelper.so")
            ("libc" "gcc:lib" "zlib"))
           (,,(string-append plugin-dir "/UnityFbxSdkNative.so")
            ("libc" "gcc:lib" "libstdc++" "libxml2" "zlib"))
           (,,(string-append plugin-dir "/libsteam_api.so")
            ("mesa" "vulkan-loader"))
           ("../support/yad/64/yad"
            ("libc" "glib" "cairo" "pango" "gdk-pixbuf" "gtk+")))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'create-wrapper
	     (lambda* (#:key inputs outputs #:allow-other-keys)
	       (let* ((output (assoc-ref outputs "out"))
	              (wrapper (string-append output "/bin/a-short-hike"))
	              (share (string-append output "/share/" ,name "-" ,version "/game"))
                      (game-lib (string-append share "/" ,plugin-dir))
	              (real (string-append share "/AShortHike.x86_64"))
	              (icon (string-append share "/AShortHike_Data/Resources/"
	                                   "UnityPlayer.png")))
	         (chmod real #o755)
	         ;; The game needs to write to the config file in the
	         ;; AShortHike_Data directory next to the binary.  Thus we
	         ;; copy the binary to a writable directory and symlink
	         ;; everything else.
                 (mkdir-p (dirname wrapper))
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (which "bash") "\n"
                              "[ -n \"$XDG_DATA_HOME\" ] || XDG_DATA_HOME=\"$HOME/.local/share\"" "\n"
                              "if [ ! -e \"$XDG_DATA_HOME/a-short-hike\" ]; then" "\n"
                              "  mkdir -p \"$XDG_DATA_HOME/a-short-hike/AShortHike_Data\"" "\n"
                              "  cp \"" real "\" \"$XDG_DATA_HOME/a-short-hike/\"" "\n"
                              "  for i in " share "/AShortHike_Data/*; do" "\n"
                              "    ln -s \"$i\" \"$XDG_DATA_HOME/a-short-hike/AShortHike_Data/\"" "\n"
                              "  done" "\n"
                              "fi" "\n"
                              "cd \"$XDG_DATA_HOME/a-short-hike\"" "\n"))
                     (format p
                             (string-append "export LD_LIBRARY_PATH=\"" share ":" game-lib ":" ,lib-dir ":"))
                     (format p "${LD_LIBRARY_PATH=:+:}$LD_LIBRARY_PATH\"\n")
                     (format p (string-append "exec -a " (basename real) " " real " \"$@\"" "\n"))))
	         (chmod wrapper #o755)
	         (make-desktop-entry-file (string-append output "/share/applications/"
	                                                 "a-short-hike.desktop")
	                                  #:name "A Short Hike"
	                                  #:exec wrapper
	                                  #:icon icon
	                                  #:comment "A little exploration game about hiking up a mountain."
	                                  #:categories '("Application" "Game")))
	       #t)))))
      (inputs
       `(("cairo" ,cairo)
         ("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("gtk+" ,gtk+)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("pango" ,pango)
         ("libx11" ,libx11)
         ("libxcursor" ,libxcursor)
         ("libxext" ,libxext)
         ("libxi" ,libxi)
         ("libxinerama" ,libxinerama)
         ("libxrandr" ,libxrandr)
         ("libxscrnsaver" ,libxscrnsaver)
         ("libxxf86vm" ,libxxf86vm)
         ("libxml2" ,libxml2)
         ("mono" ,mono)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)
         ("vulkan-loader" ,vulkan-loader)
         ("zlib" ,zlib)))
      (home-page "https://ashorthike.com/")
      (synopsis "A little exploration game about hiking up a mountain")
      (description "Hike, climb, and soar through the peaceful mountainside
landscapes of Hawk Peak provincial park.

Follow the marked trails or explore the backcountry as you make your way to
the summit.

Along the way, meet other hikers, discover hidden treasures, and take in the
world around you.")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
