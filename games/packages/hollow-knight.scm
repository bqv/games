;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2019, 2021 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages hollow-knight)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix utils)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (games humble-bundle))

(define-public hollow-knight
  (let* ((version "1432")
         (file-name (string-append "NoDRM_Linux_64_" version ".zip")))
    (package
      (name "hollow-knight")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0jy6d7ph2zgm40v2dkrbalzrl3cc719bzry8xnjpicq7173gz9vh"))))
      (build-system binary-build-system)
      (supported-systems '("x86_64-linux"))
      (arguments
       `(#:patchelf-plan
         '(("NoDRM_Linux_64/hollow_knight.x86_64"
            ("gcc:lib" "libstdc++"))
           ("NoDRM_Linux_64/hollow_knight_Data/Mono/x86_64/libmono.so")
           ("NoDRM_Linux_64/hollow_knight_Data/Mono/x86_64/libMonoPosixHelper.so"
            ("zlib"))
           ("NoDRM_Linux_64/hollow_knight_Data/Plugins/x86_64/libCSteamworks.so"
            ("libstdc++"))
           ("NoDRM_Linux_64/hollow_knight_Data/Plugins/x86_64/libsteam_api.so"
            ("libstdc++"))
           ("NoDRM_Linux_64/hollow_knight_Data/Plugins/x86_64/ScreenSelector.so"
            ("gcc:lib" "gdk-pixbuf" "glib" "gtk+-2" "libstdc++" "zlib")))
         #:install-plan
         `(("NoDRM_Linux_64/hollow_knight.x86_64" "share/hollow-knight/")
           ("NoDRM_Linux_64/hollow_knight_Data/Mono/x86_64"
            "share/hollow-knight/hollow_knight_Data/Mono/")
           ("NoDRM_Linux_64/hollow_knight_Data/Plugins"
            "share/hollow-knight/hollow_knight_Data/"))
         #:phases
         (modify-phases %standard-phases
           ;; The game is too big (> 7 GiB) to be reasonably extracted to /tmp.
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke "unzip"
                       (assoc-ref inputs "source")
                       "NoDRM_Linux_64/hollow_knight.x86_64"
                       "NoDRM_Linux_64/hollow_knight_Data/Mono/x86_64/*"
                       "NoDRM_Linux_64/hollow_knight_Data/Plugins/*")))
           (add-before 'install 'extract-resource
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (share (string-append output "/share/hollow-knight"))
                      (source (assoc-ref inputs "source")))
                 (mkdir-p share)
                 (invoke "unzip" source "-d" share)
                 (rename-file (string-append share "/NoDRM_Linux_64/hollow_knight_Data")
                              (string-append share "/hollow_knight_Data"))
                 (delete-file-recursively (string-append share "/NoDRM_Linux_64"))
                 #t)))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/hollow-knight"))
                      (share (string-append output "/share/hollow-knight"))
                      (game-lib (string-append share "/hollow_knight_Data/Plugins/x86_64"))
                      (real (string-append share "/hollow_knight.x86_64"))
                      (icon (string-append share "/hollow_knight_Data/Resources/"
                                           "UnityPlayer.png")))
                 (chmod real #o755)
                 ;; The game needs to write to the config file the
                 ;; hollow_knight_Data directory next to the binary.  Thus we
                 ;; copy the binary to a writable directory and symlink
                 ;; everything else.
                 (mkdir-p (dirname wrapper))
                 (call-with-output-file wrapper
                   (lambda (p)
                     (format p
                             (string-append
                              "#!" (which "bash") "\n"
                              "[ -n \"$XDG_DATA_HOME\" ] || XDG_DATA_HOME=\"$HOME/.local/share\"" "\n"
                              "if [ ! -e \"$XDG_DATA_HOME/hollow-knight\" ]; then" "\n"
                              "  mkdir -p \"$XDG_DATA_HOME/hollow-knight/hollow_knight_Data\"" "\n"
                              "  cp \"" real "\" \"$XDG_DATA_HOME/hollow-knight/\"" "\n"
                              "  for i in " share "/hollow_knight_Data/* ; do" "\n"
                              "    ln -s \"$i\" \"$XDG_DATA_HOME/hollow-knight/hollow_knight_Data/\"" "\n"
                              "  done" "\n"
                              "fi" "\n"
                              "cd \"$XDG_DATA_HOME/hollow-knight\"" "\n"))
                     (format p
                             (string-append "export LD_LIBRARY_PATH=\"" game-lib ":"))
                     (for-each
                      (lambda (lib) (format p "~a:"
                                            (string-append (assoc-ref inputs lib) "/lib")))
                      '("eudev" "libxext" "mesa" "pulseaudio"))
                     (format p "${LD_LIBRARY_PATH=:+:}$LD_LIBRARY_PATH\"\n")
                     (format p (string-append "exec -a " (basename real) " " real " \"$@\"" "\n"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file (string-append output "/share/applications/"
                                                         "hollow-knight.desktop")
                                          #:name "Hollow Knight"
                                          #:exec wrapper
                                          #:icon icon
                                          #:comment "An atmospheric adventure through a surreal, bug-infested world"
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("unzip" ,unzip)))
      (inputs
       `(("eudev" ,eudev)
         ("gcc:lib" ,gcc "lib")
         ("gdk-pixbuf" ,gdk-pixbuf)
         ("glib" ,glib)
         ("gtk+-2" ,gtk+-2)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("libxext" ,libxext)
         ("mesa" ,mesa)
         ("pulseaudio" ,pulseaudio)))
      (home-page "https://hollowknight.com/")
      (synopsis "An atmospheric adventure through a surreal, bug-infested world")
      (description "Hollow Knight is a 2D Metroidvania action-adventure game,
that takes place in Hallownest, a fictional ancient kingdom.  The player
controls an insect-like, silent, and nameless knight while exploring the
underground world.  The knight wields a nail, which is a cone-shaped sword,
used both in combat and environmental interaction.")
      (license (undistributable "No URL")))))

(define-public gog-hollow-knight
  (let ((buildno "23987"))
    (package
      (inherit hollow-knight)
      (name "gog-hollow-knight")
      (version "1.4.3.2")
      (source
       (origin
        (method gog-fetch)
        (uri "gogdownloader://hollow_knight/en3installer0")
        (file-name (string-append "hollow_knight_"
                                  (string-replace-substring version "." "_")
                                  "_" buildno ".sh"))
        (sha256
         (base32
          "19g0b6mzjahvj1y3mk25li61wardgk4fnl5cn9v24s9lhq8i8d28"))))
      (supported-systems '("x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       (substitute-keyword-arguments
         (strip-keyword-arguments '(#:install-plan) (package-arguments hollow-knight))
         ((#:patchelf-plan plan)
          `'(("hollow_knight.x86_64"
              ("gcc:lib" "libstdc++"))
             ("hollow_knight_Data/Mono/x86_64/libmono.so")
             ("hollow_knight_Data/Mono/x86_64/libMonoPosixHelper.so"
              ("zlib"))
             ("hollow_knight_Data/Plugins/x86_64/libCSteamworks.so"
              ("libstdc++"))
             ("hollow_knight_Data/Plugins/x86_64/libsteam_api.so"
              ("libstdc++"))
             ("hollow_knight_Data/Plugins/x86_64/ScreenSelector.so"
              ("gcc:lib" "gdk-pixbuf" "glib" "gtk+-2" "libstdc++" "zlib"))))
         ((#:phases phases)
          `(modify-phases %standard-phases
             (add-after 'install 'create-wrapper
	           (lambda* (#:key inputs outputs #:allow-other-keys)
	             (let* ((output (assoc-ref outputs "out"))
	                    (wrapper (string-append output "/bin/hollow-knight"))
	                    (share (string-append output "/share/gog-hollow-knight-" ,version "/game"))
                        (game-lib (string-append share "/hollow_knight_Data/Plugins/x86_64"))
	                    (real (string-append share "/hollow_knight.x86_64"))
	                    (icon (string-append share "/hollow_knight_Data/Resources/"
	                                         "UnityPlayer.png")))
	               (chmod real #o755)
	               ;; The game needs to write to the config file in the
	               ;; hollow_knight_Data directory next to the binary.  Thus we
	               ;; copy the binary to a writable directory and symlink
	               ;; everything else.
                   (mkdir-p (dirname wrapper))
                   (call-with-output-file wrapper
                     (lambda (p)
                       (format p
                               (string-append
                                "#!" (which "bash") "\n"
                                "[ -n \"$XDG_DATA_HOME\" ] || XDG_DATA_HOME=\"$HOME/.local/share\"" "\n"
                                "if [ ! -e \"$XDG_DATA_HOME/hollow-knight\" ]; then" "\n"
                                "  mkdir -p \"$XDG_DATA_HOME/hollow-knight/hollow_knight_Data\"" "\n"
                                "  cp \"" real "\" \"$XDG_DATA_HOME/hollow-knight/\"" "\n"
                                "  for i in " share "/hollow_knight_Data/* ; do" "\n"
                                "    ln -s \"$i\" \"$XDG_DATA_HOME/hollow-knight/hollow_knight_Data/\"" "\n"
                                "  done" "\n"
                                "fi" "\n"
                                "cd \"$XDG_DATA_HOME/hollow-knight\"" "\n"))
                       (format p
                               (string-append "export LD_LIBRARY_PATH=\"" game-lib ":"))
                       (for-each
                        (lambda (lib) (format p "~a:"
                                              (string-append (assoc-ref inputs lib) "/lib")))
                        '("eudev" "libxext" "mesa" "pulseaudio"))
                       (format p "${LD_LIBRARY_PATH=:+:}$LD_LIBRARY_PATH\"\n")
                       (format p (string-append "exec -a " (basename real) " " real " \"$@\"" "\n"))))
	               (chmod wrapper #o755)
	               (make-desktop-entry-file (string-append output "/share/applications/"
	                                                       "hollow-knight.desktop")
	                                        #:name "Hollow Knight"
	                                        #:exec wrapper
	                                        #:icon icon
	                                        #:comment "An atmospheric adventure through a surreal, bug-infested world"
	                                        #:categories '("Application" "Game")))
	             #t)))))))))
