;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;; Copyright © 2021 Olivier Rojon <o.rojon@posteo.net>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages the-dark-mod)
  #:use-module (ice-9 match)
  #:use-module (guix i18n)
  #:use-module ((guix licenses) :prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system scons)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages xorg))

;; TODO: The Dark Mod is free software, so technically this package can be moved
;; to upstream Guix once the game data distribution is versioned.

(define the-dark-mod-updater
  (let ((scons-flags
         (string-append "TARGET_ARCH="
                        (match (or (%current-target-system)
                                   (%current-system))
                          ("i686-linux" "x86")
                          (_ "x64"))))
        (the-dark-mod-env-var-name "THEDARKMOD_PATH")
        (the-dark-mod-env-var-value "~/.local/share/darkmod"))
    (package
      (name "the-dark-mod-updater")
      (version "2.09a")
      (source (origin
                (method url-fetch)
                (uri (string-append "https://www.thedarkmod.com/sources/thedarkmod."
                                    version ".src.7z"))
                (sha256
                 (base32
                  ;; TODO: Hash seems unreliable, find a proper download
                  ;; source or report upstream.
                  "0schiq9rs6ymnrxlxvyjzni3h1fa86w7phjfxfy1j6bfq395wbwp"))))
      (build-system scons-build-system)
      (arguments
       `(#:tests? #f                    ;no test
         #:scons ,scons-python2
         ;; BUILD=release makes Scons strip the executable, which fails because
         ;; "strip" is not found in the path.
         #:scons-flags ;; (list "-j" "1"  "CXXFLAGS=-std=c++14" ,scons-flags )
         (list ,scons-flags )
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key source #:allow-other-keys)
               (invoke "7z" "x" "-osource" source )
               (chdir "source/tdm_update")
               #t))
           (add-after 'unpack 'fix-include
             (lambda _
               (substitute* "ConsoleUpdater.h"
                 (("#include <signal.h>" all)
                  (string-append all "\n#include <math.h>")))
               #t))
           (replace 'install
             (lambda* (#:key inputs outputs system #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (updater (string-append out "/bin/tdm_update"))
                      (updater-real (string-append out "/bin/.tdm_update-real"))
                      (arch (if (string= system "i686-linux") "x86" "x64")))
                 (mkdir-p (string-append out "/bin"))
                 (copy-file (format #f "build/scons_~a/release/update" arch)
                            updater-real)
                 (with-output-to-file updater
                   (lambda _
                     (format #t "\
#!~a
~a=${~a:-~a}
mkdir -p \"$~a\"
## tdm_update outputs a log in its current working directory.
cd \"$~a\"
~a --noselfupdate --targetdir \"$~a\" \"$@\"~%"
                             (which "bash")
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-value
                             ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-name
                             updater-real
                             ,the-dark-mod-env-var-name)))
                 (chmod updater #o555)
                 #t))))))
      (native-inputs
       `(("p7zip" ,p7zip)))
      (home-page "https://www.thedarkmod.com/")
      (synopsis "Game based on the Thief series by Looking Glass Studios")
      (description (format #f (G_ "The Dark Mod (TDM) is stealth/infiltration game
based on the Thief series by Looking Glass Studios.  Formerly a Doom 3 mod,
it is now released as a standalone.

The game data must be fetched manually by running @command{tdm_update}.
The ~a environment variable specifies the location where the game data is
saved (defaults to ~a).")
                           the-dark-mod-env-var-name the-dark-mod-env-var-value))
      (supported-systems '("x86_64-linux" "i686-linux"))
      (license (list license:gpl3       ; idTech 4 engine
                     license:bsd-3 ; Portion of the engine by Broken Glass Studios
                     ;; All other non-software components: they are not
                     ;; included in the Guix package, but the updater fetches
                     ;; them.
                     license:cc-by-sa3.0)))))

(define-public the-dark-mod
  (let ((the-dark-mod-env-var-name "THEDARKMOD_PATH")
        (the-dark-mod-env-var-value "~/.local/share/darkmod"))
    (package
      (inherit the-dark-mod-updater)
      (name "the-dark-mod")
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f                    ;no test
         #:configure-flags (list "-DCOPY_EXE=OFF")
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key source #:allow-other-keys)
               (invoke "7z" "x" "-osource" source )
               (chdir "source")
               #t))
           (replace 'install
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (mesa (assoc-ref inputs "mesa"))
                      (alsa (assoc-ref inputs "alsa-lib"))
                      (bin (string-append out "/bin/thedarkmod"))
                      (bin-real (string-append out "/bin/.thedarkmod-real"))
                      (updater (string-append (assoc-ref inputs "updater") "/bin/tdm_update"))
                      (apps (string-append out "/share/applications"))
                      (icons (string-append out "/share/icons")))
                 (mkdir-p (string-append out "/bin"))
                 (copy-file ,@(match (%current-system)
                                ("i686-linux"
                                 '("thedarkmod.x86"))
                                ("x86_64-linux"
                                 '("thedarkmod.x64")))
                            bin-real)
                 (symlink updater (string-append out "/bin/" (basename updater)))
                 (with-output-to-file bin
                   (lambda _
                     (format #t "\
#!~a
export LD_LIBRARY_PATH=~a/lib:~a/lib
~a=${~a:-~a}
echo ~a=$~a
[ ! -d \"$~a\" ] && echo \"Can't start with an empty ~a.  Run tdm_update to populate it.\" && exit 1
cd \"$~a\"
exec -a \"~a\" ~a \"$@\"\n"
                             (which "bash")
                             mesa alsa
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name ,the-dark-mod-env-var-value
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-name ,the-dark-mod-env-var-name
                             ,the-dark-mod-env-var-name
                             (basename bin-real) bin-real)))
                 (chmod bin #o555)
                 (mkdir-p apps)
                 (mkdir-p icons)
                 (install-file "../source/tdm_update/darkmod.ico" icons)
                 (with-output-to-file
                     (string-append apps "/darkmod.desktop")
                   (lambda _
                     (format #t
                             "[Desktop Entry]~@
                     Name=The Dark Mod~@
                     Comment=The Dark Mod~@
                     Exec=~a~@
                     TryExec=~@*~a~@
                     Icon=darkmod~@
                     Categories=Game~@
                     Type=Application~%"
                             bin)))))))))
      (inputs
       `(("mesa" ,mesa)
         ("libxxf86vm" ,libxxf86vm)
         ("openal" ,openal)
         ("alsa-lib" ,alsa-lib)
         ("libxext" ,libxext)
         ("updater" ,the-dark-mod-updater)))
      (native-inputs
       `(("p7zip" ,p7zip)
         ("m4" ,m4)
         ("subversion" ,subversion))))))
